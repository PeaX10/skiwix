<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public function activities(){
        return $this->hasMany('App\Activity');
    }

    public function photos(){
        return $this->hasMany('App\Photo');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function isAdmin(){
        if($this->bde == 1) return true; else return false;
    }

    public function addPts($pts){
        $this->points += $pts;
        $this->save();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstame', 'lastname', 'points', 'room', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
