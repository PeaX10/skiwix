<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use App\Activity;
use App\Article;
use App\Photo;
use App\Comment;
use ZanySoft\Zip\Zip;
use Carbon\Carbon;
use App\Product;
use App\Order;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        Carbon::setLocale('fr');
    }

    public function index(){
        $user = Auth::user();
        $ranking = User::orderBy('points', 'desc')->where('bde', 0)->take(20)->get();
        $top = $ranking->take(3);
        $slides = Article::orderBy('created_at', 'desc')->where('state', 1)->take(3)->get();
        $activities = Activity::where('state', '>=', 0)->where('state', '<', 2)->get();
        return view('home', compact('user', 'activities', 'top', 'ranking', 'slides'));
    }

    public function login(){
        if(Auth::check())
            return redirect('logout');
        else
            return view('auth.login');
    }

    public function postLogin(Request $request){
        $rules = [  'email' => 'required|email',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
                return redirect()->intended('/');
            }else{
                $validator->getMessageBag()->add('password', 'Mot de passe incorrect !');
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }
    }

    public function cards(){

        set_time_limit(10000000);
        $pdf = \App::make('dompdf.wrapper');
        return $pdf->loadView('qrcard')->download('cards.pdf');

        return view('qrcard');
    }

    public function qrCode($id){
        if(User::where('qrcode_id', $id)->count() > 0){
            // On check si on a une activité en cours
            $user = Auth::user();
            $qr_user = User::where('qrcode_id', $id)->firstOrFail();
            $activity = Activity::where('state', 0)->where('user_id',$user->id)->orWhere('state', 0)->where('helpers', 'like', '%'.$user->id.'%')->orderBy('id', 'desc')->first();
            if(count($activity) > 0){
                $members = json_decode($activity->members, true);
                foreach($members as $key => $team){
                    foreach($team as $key2 => $member){
                        $members[$key][$key2] = $this->getName($member);
                    }
                }
                return view('admin.activity.add_member', compact('user', 'members', 'activity', 'qr_user'));
            }else{
                return redirect()->back();
            }
        }else{
            return view('auth.create');
        }
    }

    public function getName($id){
        $user = User::find($id);
        return $user->firstname.' '.$user->lastname;
    }

    public function createUser(Request $request, $id){
        $rules = [
            'firstname' => 'required|alpha_dash',
            'lastname' => 'required|alpha_dash',
            'password' => 'required|same:password_confirm|min:3',
            'avatar' => 'image|max:4096',
            'email' => 'required|email|unique:users',
            'room' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            // Tout c'est bien passé, on enregistre tout ça et on le connecte sur le site
            $user = new User();
            $user->firstname = ucfirst(Input::get('firstname'));
            $user->lastname = ucfirst(Input::get('lastname'));
            $user->password = Hash::make(Input::get('password'));
            $user->email = Input::get('email');
            $user->room = Input::get('room');
            if($request->hasFile('avatar')){
                $name_avatar = 'avatar_'.str_random('6').time();
                Image::make(Input::file('avatar'))->resize(140, 140)->save('uploads/avatar/'.$name_avatar.'.jpg');
                $user->avatar = $name_avatar;
            }
            $user->qrcode_id = $id;
            if(!User::where('qrcode_id', $id)->exists()){
                $user->save();
                if(Auth::login($user, true)){
                    return redirect('/');
                }
            }else{
                return redirect('/');
            }

            return redirect('/')->with('contact_message_success', 'true');
        }
    }

    public function blog(){
        $articles = Article::where('state', 1)->orderBy('created_at', 'desc')->paginate(6);
        $slides = $articles->take(3);
        $user = Auth::user();
        return view('blog.index', compact('user', 'articles', 'slides'));
    }

    public function rank(){
        $user = Auth::user();
        $ranking = User::orderBy('points', 'desc')->where('bde', 0)->get();
        $top = $ranking->take(3);
        return view('rank', compact('user', 'top', 'ranking'));
    }

    public function pass(){
        $user = Auth::user();
        return view('pass', compact('user'));
    }

    public function showArticle($id){
        $user = Auth::user();
        $article = Article::find($id);
        if(empty($article) or $article->state == 0) return view('404');
        else return view('blog.show', compact('user', 'article'));
    }

    public function activity(){
        $user = Auth::user();
        $p_activities = Activity::where('state', ''-1)->get();
        $c_activities = Activity::where('state', '>=', 0)->where('state', '<', 2)->get();
        foreach($p_activities as $key => $value){
            if(!$value->present($user->id)) $p_activities->forget($key);
        }
        return view('activity.index', compact('user', 'p_activities', 'c_activities'));
    }

    public function showActivity($id){
        $user = Auth::user();
        $activity = Activity::find($id);
        if($activity->state > -2) return view('activity.show', compact('user', 'activity'));
        else return view('404');
    }

    public function postActivityPhoto(Request $request, $id){
        $rules = [  'file' => 'image|max:2048'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return abort(404);
        }else{
            $name = 'p_'.str_random(6).time().'.jpg';
            $photo = new Photo();
            $photo->name = $name;
            $photo->user_id = Auth::user()->id;
            $photo->activity_id = $id;
            $photo->save();
            $photo = Image::make(Input::file('file'))->save('uploads/activity/photo/'.$name);
            echo 'SUCCESS';
        }
    }

    public function downloadPhoto($id){ // Une seule photo
        $photo = Photo::find($id);
        return response()->download(public_path('uploads/activity/photo/'.$photo->name));
    }

    public function downloadPhotos($id){ // Toutes les photos d'une activité
        $activity = Activity::find($id);
        $photos = $activity->photos()->get();
        $name = camel_case($activity->name).'_'.str_random(3).'.zip';
        $zip = Zip::create($name);
        foreach($photos as $photo){
            $zip->add(public_path('/uploads/activity/photo/'.$photo->name));
        }
        $zip->close();
        return response()->download($name)->deleteFileAfterSend(true);;
    }

    public function deletePhoto($id){
        $photo = Photo::find($id);
        if($photo->user->bde == 1 or $photo->user->id==Auth::user()->id){
            // Si il fait parti de la liste ou bien que la photo lui appartient
            $photo->delete();
            return redirect()->back();
        }else{
            abort(404);
        }
    }

    public function deleteComment($id){
        $comment = Comment::find($id);
        if(Auth::user()->bde == 1 or $comment->user->id==Auth::user()->id){
            // Si il fait parti de la liste ou bien que la photo lui appartient
            if($comment->type == 'activity'){
                $type = 'activity';
                $id = $comment->activity_id;
            }else{
                $type = 'blog';
                $id = $comment->article_id;
            }
            $comment->delete();
            return redirect(url($type.'/'.$id.'/show#comments'));
        }else{
            abort(404);
        }
    }

    public function postActivityComment(Request $request, $id){
        $rules = [  'comment' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return abort(404);
        }else{
            $comment = new Comment();
            $comment->type = 'activity';
            $comment->activity_id = $id;
            $comment->user_id = Auth::user()->id;
            $comment->content = Input::get('comment');
            $comment->save();
            return redirect(url('activity/'.$id.'/show#comments'));
        }
    }

    public function postArticleComment(Request $request, $id){
        $rules = [  'comment' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return abort(404);
        }else{
            $comment = new Comment();
            $comment->type = 'article';
            $comment->article_id = $id;
            $comment->user_id = Auth::user()->id;
            $comment->content = Input::get('comment');
            $comment->save();
            return redirect(url('blog/'.$id.'/show#comments'));
        }
    }

    public function shop(){
        $user = Auth::user();
        $products = Product::where('stock', '!=', 0)->get();
        return view('shop.index', compact('user', 'products'));
    }

    public function showProduct($id){
        $user = Auth::user();
        $product = Product::find($id);
        $options = (array) json_decode($product->options, true);
        return view('shop.show', compact('user', 'product', 'options'));
    }

    public function postOrder(Request $request){
        $rules = [
            'product_id' => 'required',
            'quantity' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return abort(404);
        }else{
            $order = ['product_id' => Input::get('product_id'), 'quantity' => Input::get('quantity')];
            if($request->has('option')) $order['option'] = Input::get('option'); else $order['option'] = null;
            if(!empty($request->cookie('shop'))) $basket = json_decode($request->cookie('shop'), true); else $basket = array();
            $basket[] = $order;
            Cookie::queue('shop', json_encode($basket), 120);
            return redirect()->back();
        }
    }

    public function about(){
        $user = Auth::user();
        return view('about', compact('user'));
    }

    public function generateImage(){
        for($i=408; $i < 450; $i++){
            $qr = QrCode::format('png')->size(400 )->generate('http://skiwix.fr/qrCode/'.($i));
            Image::make($qr)->save('uploads/qrcode/'.$i.'.png');
        }
    }

    public function addBitcouix(){
        $users = User::all();
        $user = Auth::user();
        return view('admin.addbitcouix', compact('user','users'));
    }

    public function addBitcouixPost(Request $request){
        $rules = [  'user' => 'required',
            'bitcouix' => 'required|min:1|max:250'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back();
        }else{
            $user = User::find(Input::get('user'));
            $user->addPts(Input::get('bitcouix'));
            return redirect()->back()->withErrors(['bitcouix_added', "success"]);
        }
    }

    public function myOrders(){
        $user = Auth::user();
        $orders = $user->orders()->orderBy('id', 'desc')->get();
        return view('shop.order', compact('orders', 'user'));
    }

    public function basket(){
        $user = Auth::user();
        $orders = json_decode(Cookie::get('shop'), true);
        return view('shop.basket', compact('orders', 'user'));
    }

    public function deleteItem($id){
        $orders = json_decode(Cookie::get('shop'), true);
        if(!empty($orders[$id])) unset($orders[$id]);
        Cookie::queue('shop', json_encode($orders), 120);
        return redirect()->back();
    }

    public function doShop(Request $request){
        $user = Auth::user();
        if($request->has('skin') && is_numeric(Input::get('skin'))){
            $order = new Order();
            $order->user_id = $user->id;
            $order->data = Cookie::get('shop');
            foreach(json_decode(Cookie::get('shop'), true) as $order2){
                $product = Product::find($order2['product_id']);
                if($product->stock != -1){
                    $product->stock -= $order2['quantity'];
                    $product->save();
                }
            }
            $order->skin = Input::get('skin');
            $order->state = 0;
            $order->save();
            Cookie::queue('shop', null, 120);
            return redirect('shop/orders');
        }else{
            abort(404);
        }
    }

    public function updatePhoto(){
        $user = Auth::user();
        return view('updatePhoto', compact('user'));
    }

    public function updatePhotoPost(Request $request){
        $user = Auth::user();
        $rules = [
            'avatar' => 'required|image'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return abort(404);
        }else{
            $name_avatar = 'avatar_'.str_random('6').time();
            Image::make(Input::file('avatar'))->resize(140, 140)->save('uploads/avatar/'.$name_avatar.'.jpg');
            $user->avatar = $name_avatar;
            $user->save();
            return redirect()->back();
        }
    }
}
