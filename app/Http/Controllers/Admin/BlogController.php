<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('id', 'desc')->get();
        $user = Auth::user();
        return view('admin.blog.index', compact('user', 'articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('admin.blog.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'content' => 'required',
            'image' => 'required|image',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $article = new Article();
            $article->name = Input::get('name');
            if($request->has('state') && Input::get('state') == 'on') $article->state = 1; else $article->state = 0;
            $article->description = Input::getcontent('description');
            $article->content = Input::get('content');
            $article->save();
            Image::make(Input::file('image'))->save(public_path('uploads/article/'.$article->id.'.jpg'));
            return redirect('admin/blog');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $article = Article::find($id);
        return view('admin.blog/edit', compact('user', 'article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'content' => 'required',
            'image' => 'image',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $article = Article::find($id);
            $article->name = Input::get('name');
            if($request->has('state') && Input::get('state') == 'on') $article->state = 1; else $article->state = 0;
            $article->description = Input::getcontent('description');
            $article->content = Input::get('content');
            $article->save();
            if($request->hasFile('image')) Image::make(Input::file('image'))->save(public_path('uploads/article/'.$article->id.'.jpg'));
            return redirect('admin/blog');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id){
        $article = Article::find($id);
        $article->delete();
        return redirect()->back();
    }
}
