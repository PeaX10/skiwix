<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use App\User;
use  Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $activities = Activity::orderBy('id', 'desc')->get();
       $user = Auth::user();
       return view('admin.activity.index', compact('user', 'activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('admin.activity.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'activity_id' => 'required|numeric',
            'team' => 'required|numeric',
            'max_members' => 'required|numeric',
            'point_winner' => 'required|numeric',
            'point_participant' => 'required|numeric'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $activity  = new Activity();
            switch(Input::get('activity_id')){
                case 1:
                    $name = '50 mètres JAGERMEISTIX';
                    $description = 'Relais avec en prime des shoots de Jager';
                    break;
                case 2:
                    $name = 'Concours de Barde (karaoké ?)';
                    $description = 'Oh non ! Bouchez vous les oreilles ! Assurancetourix et ses cousins vont encore massacrer nos chansons préférées ! Venez nous sauver et relever le niveau en prenant part au Karaoké ! ';
                    break;
                case 3:
                    $name = 'Concours de Luge';
                    $description = 'Viens glisser sur les pistes avec nous';
                    break;
                case 4:
                    $name = 'Lancer de romain';
                    $description = 'Viens lancer du romain';
                    break;
                case 5:
                    $name = 'Rugby Gaulois';
                    $description = 'Joue eu rugby avec notre bon vieux champion Obélix';
                    break;
                case 6:
                    $name = 'Tir à la corde';
                    $description = 'Affrontez vous en équipe pour rafler un maximum de bitcouix !';
                    break;
                case 7:
                    $name = 'Défi photo - Menhir en neige';
                    $description = 'Nous les gaulois, on adooore rigoler ! Et comme on sait que vous êtes de ptits rigolots nous vous proposons le défi suivant : celui de la photo la plus insolite ! L’auteur de la photo qui récoltera le plus de rire des gaulois sera récompensé ! ';
                    break;
                case 8:
                    $name = 'Défi photo (à actualiser chaque jour)';
                    $description = 'Nous les gaulois, on adooore rigoler ! Et comme on sait que vous êtes de ptits rigolots nous vous proposons le défi suivant : celui de la photo la plus insolite ! L’auteur de la photo qui récoltera le plus de rire des gaulois sera récompensé ! ';
                    break;
                case 9:
                    $name = 'Lancer de poisson pas frais';
                    $description = 'Viens lancer des poissons avec Ordralfabétix afin d\'atteindre un maximum de cibles !';
                    break;
                default:
                    $name = 'ERROR NO ACTIVITY NAME';
                    $description = '';
                    break;
            }
            $activity->name = $name;
            $activity->description  = $description;
            $team = array();
            for($k = 1; $k <= Input::get('team'); $k++){
                $team[] = [];
            }
            $activity->members = json_encode($team);
            $activity->max_members = Input::get('max_members');
            $activity->point_winner = Input::get('point_winner');
            $activity->point_participant = Input::get('point_participant');
            $activity->state = 0;
            $location = ['type' => 'map', 'data' => ['lat' => Input::get('lat'), 'lng' => Input::get('lng')]];
            if($request->exists('locText')) $location['data']['text'] = Input::get('locText');
            $activity->location  = json_encode($location);
            $activity->user_id = Auth::user()->id;
            $helpers = [];
            if($request->exists('helpers')){
                foreach(Input::get('helpers') as $helper){
                    $helpers[] = $helper;
                }
            }
            $activity->helpers = json_encode($helpers);
            $activity->save();
            Image::make('uploads/activity/'.Input::get('activity_id').'.png')->save('uploads/activity/'.$activity->id.'.jpg');

            return redirect('activity/'.$activity->id.'/show');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function duplicate($id){
        $activity = Activity::find($id);
        $newTask = $activity->replicate();
        $newTask->save();
        Image::make(public_path('uploads/activity/'.$activity->id.'.jpg'))->save('uploads/activity/'.$newTask->id.'.jpg');
        return redirect()->back();
    }

    public function delete($id){
        $activity = Activity::find($id);
        $activity->delete();
        return redirect()->back();
    }

    public function addUserToTeam($activity_id, $user_id, $team_id){
        $user = Auth::user();
        $activity = Activity::find($activity_id);
        // Revérification pour la partie administrateur
        if($user->bde == 1){
            if(!$activity->in_members($user_id)){
                $activity->addToTeam($user_id, $team_id);
                return redirect('activity/'.$activity_id.'/show');
            }else{
                dd('Cet utilisateur participe déjà à l\'activité');
            }
        }else{
            abort(404);
        }
    }

    public function deleteUserToTeam($activity_id, $user_id, $team_id){
        $user = Auth::user();
        $activity = Activity::find($activity_id);
        // Revérification pour la partie administrateur
        if($user->bde == 1){
            $members = json_decode($activity->members, true);
            foreach($members[$team_id] as $key => $value){
                if($value == $user_id) unset($members[$team_id][$key]);
            }
            $activity->members = json_encode($members);
            $activity->save();
            return redirect()->back();
        }else{
            abort(404);
        }
    }

    public function startStop($id){
        $activity = Activity::find($id);
        if($activity->state == 0){
            $activity->state += 1;
            $activity->save();
        }
        return redirect()->back();
    }

    public function winnerTeam($id_activity, $id_team){
        $activity = Activity::find($id_activity);
        if($activity->state == 1){
            $activity->state = 2;
            $activity->winner_team = $id_team;
            $activity->save();

            foreach(json_decode($activity->members, true) as $key => $team){
                if($key == $id_team) $pts = $activity->point_winner; else $pts = $activity->point_participant;
                foreach($team as $member){
                    $mbm = User::find($member);
                    $mbm->points += $pts;
                    $mbm->save();
                }
            }

            return redirect()->back();
        }else{
            abort(404);
        }

    }

    public function addMember(){
        $user = Auth::user();
        return view('admin.activity.man_add_member', compact('user'));
    }

}
