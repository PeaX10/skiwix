<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    public function daily(){
        $user = Auth::user();
        $count = 0;
        foreach($user->orders()->whereDate('created_at', DB::raw('CURDATE()'))->get() as $order){
            foreach((array) json_decode($order->data, true) as $product){
                if($product['product_id'] == $this->id) $count += $product['quantity'];
            }
        }
        return $count;
    }
}
