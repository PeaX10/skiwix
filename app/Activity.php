<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function team($id=null){
        if(empty($id)){
            return count(json_decode($this->members));
        }else{
            return json_decode($this->members)[$id];
        }
    }

    public function countMembers(){
        $count = 0;
        $members = json_decode($this->members);
        foreach($members as $key_team => $team){
            $count += count($team);
        }
        return $count;
    }

    public function locationInfo(){
        return (array) json_decode($this->location, true);
    }

    public function present($id){
        $present = false;
        $members = json_decode($this->members);
        foreach ($members as $key => $team){
            if(in_array($id, $team)) $present = true;
        }
        return $present;
    }

    public function photos(){
        return $this->hasMany('App\Photo');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function helpers(){
        return $this->hasMany('App\User', 'helpers');
    }

    public function in_members($user_id){
        $verif = false;
        $members = json_decode($this->members, true);
        foreach($members as $team){
            foreach($team as $member){
                if($member == $user_id) $verif = true;
            }
        }

        return $verif;
    }

    public function addToTeam($user_id, $team_id){
        $members = json_decode($this->members, true);
        $members[$team_id][] = $user_id;
        $this->members = json_encode($members);
        $this->save();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'image', 'participants', 'state', 'location',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
