<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('members')->nullable();
            $table->string('max_members')->default(0);
            $table->string('point_winner')->default(100);
            $table->string('point_participant')->default(10);
            $table->integer('state')->default(-2);
            $table->string('location');
            $table->integer('winner_team')->default(-1);
            $table->integer('user_id');
            $table->string('helpers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
