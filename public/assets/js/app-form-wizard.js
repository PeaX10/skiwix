var App = (function () {
  'use strict';
  
  App.wizard = function( ){

    //Fuel UX
    $('.wizard-ux').wizard();

    $('.wizard-ux').on('changed.fu.wizard',function(){
      //Bootstrap Slider
      $('.bslider').slider();
    });
    
    $(".wizard-next").click(function(e){
      var id = $(this).data("wizard");
      $(id).wizard('next');
      e.preventDefault();
    });
    
    $(".wizard-previous").click(function(e){
      var id = $(this).data("wizard");
      $(id).wizard('previous');
      e.preventDefault();
    });


   //Select2
    $(".select2").select2({
      width: '100%'
    });
    
    //Select2 tags
    $(".tags").select2({tags: true, width: '100%'});

    $("#looserPts_slider").slider({
        value: 50,
        min: 1,
        max: 200
    }).on("slide",function(e){
      $("#looserPts").html(e.value);
    });

    $("#winnerPts_slider").slider({
        value: 100,
        min: 1,
        max: 200
    }).on("slide",function(e){
      $("#winnerPts").html(e.value);
    });

    $("#maxMembers_slider").slider({
        value: 15,
        min: 2,
        max: 75
    }).on("slide",function(e){
        $("#maxMembers").html(e.value);
    });

    $("#team_slider").slider({
        value: 2,
        min: 1,
        max: 10
    }).on("slide",function(e){
        $("#team").html(e.value);
    });
    
  };

  return App;
})(App || {});
