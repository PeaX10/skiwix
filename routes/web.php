<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index')->middleware('auth');
Route::get('/login', 'Controller@login')->name('login');
Route::get('/update/photo', 'Controller@updatePhoto')->name('updatePhoto')->middleware('auth');
Route::post('/update/photo', 'Controller@updatePhotoPost')->name('updatePhoto')->middleware('auth');
Route::get('/blog', 'Controller@blog')->name('blog')->middleware('auth');
Route::get('/blog/{id}/show', 'Controller@showArticle')->middleware('auth');
Route::post('/blog/{id}/postComment', 'Controller@postArticleComment')->middleware('auth'); // Post comment
Route::get('/activity', 'Controller@activity')->name('activity')->middleware('auth');
Route::get('/activity/{id}/show', 'Controller@showActivity')->middleware('auth');
Route::post('/activity/{id}/postComment', 'Controller@postActivityComment')->middleware('auth');
Route::post('/activity/{id}/postPhoto', 'Controller@postActivityPhoto')->middleware('auth');
Route::get('/download/photo/{id}', 'Controller@downloadPhoto')->middleware('auth');
Route::get('/download/photos/{id}', 'Controller@downloadPhotos')->middleware('auth');
Route::get('/delete/photo/{id}', 'Controller@deletePhoto')->middleware('auth');
Route::get('/delete/comment/{id}', 'Controller@deleteComment')->middleware('auth');
Route::get('/about', 'Controller@about')->name('about')->middleware('auth');
Route::get('/rank', 'Controller@rank')->name('rank')->middleware('auth');
Route::get('/shop', 'Controller@shop')->name('shop')->middleware('auth');
Route::get('/shop/{id}/show', 'Controller@showProduct')->middleware('auth');
Route::get('/shop/orders', 'Controller@myOrders')->middleware('auth');
Route::get('/shop/basket', 'Controller@basket')->middleware('auth');
Route::get('/shop/{id}/delete', 'Controller@deleteItem')->middleware('auth');
Route::post('/shop/basket', 'Controller@doShop')->middleware('auth');
Route::post('/shop/postOrder', 'Controller@postOrder')->middleware('auth');
Route::get('/pass', 'Controller@pass')->name('pass')->middleware('auth');
Route::get('/generateImage', 'Controller@generateImage')->name('generateImage')->middleware('auth');
Route::post('/login', 'Controller@postLogin');
Route::get('cards', 'Controller@cards');
Route::get('qrCode/{id}', 'Controller@qrCode');
Route::post('qrCode/{id}', 'Controller@createUser');
Route::get('logout', function(){
    Illuminate\Support\Facades\Auth::logout();
    return redirect('/');
});

Route::prefix('admin')->middleware(['auth', 'admin'])->group(function () {
    Route::resource('activity', 'Admin\ActivityController');
    Route::get('activity/{id}/clone', 'Admin\ActivityController@duplicate');
    Route::get('activity/{id}/delete', 'Admin\ActivityController@delete');
    Route::get('activity/{id}/start', 'Admin\ActivityController@startStop');
    Route::get('activity/{id}/winner/{id2}', 'Admin\ActivityController@winnerTeam');
    Route::get('bitcouix/add', 'Controller@addBitcouix');
    Route::post('bitcouix/add', 'Controller@addBitcouixPost');
    Route::get('activity/{id}/addUser/{user}/{team}', 'Admin\ActivityController@addUserToTeam');
    Route::get('activity/{id}/deleteUser/{user}/{team}', 'Admin\ActivityController@deleteUserToTeam');
    Route::get('activity/{id}/addMember', 'Admin\ActivityController@addMember');
    Route::resource('blog', 'Admin\BlogController');
    Route::get('article/{id}/delete', 'Admin\BlogController@delete');
    Route::get('shop/stock', 'Admin\ShopController@stock');
    Route::post('shop/{id}/stock', 'Admin\ShopController@updateStock');
    Route::resource('shop', 'Admin\ShopController');
    Route::get('shop/{id}/delete', 'Admin\ShopController@delete');

});