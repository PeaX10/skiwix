@extends('layout.default', ['menu' => 'blog'])

@section('title') Blog @endsection

@section('content')
        <div class="main-content container-fluid">
            @if(count($slides) == 0)
                <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                    <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                    <div class="message">
                        <strong>Oupss!</strong> Il n'y a pas d'articles publiés pour le moment !
                    </div>
                </div>
            @else
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @foreach($slides as $key => $slide)
                            <li data-target="#myCarousel" data-slide-to="{{ $key }}" @if($key == 0) class="active" @endif></li>
                        @endforeach
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner slide" data-interval="3000" data-ride="carousel">
                        @foreach($slides as $key => $slide)
                            <div class="item @if($key == 0) active @endif">
                                <a href="{{ url('blog/'.$slide->id.'/show') }}">
                                    <img src="{{ url('uploads/article/'.($slide->id).'.jpg') }}" alt="{{ $slide->name }}">
                                    <div class="carousel-caption">
                                        <h3>{{ $slide->name }}</h3>
                                        <p>{{ $slide->description }}</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            @endif
            <div class="user-profile" style="margin-top:10px">
                    <div class="row">
                        <h2 style="padding-left:10px">Les dernières actualités</h2>
                        <hr>
                        @foreach($articles as $key => $article)
                            <div class="col-md-6">
                                <div class="user-display">
                                    <a href="{{ url('blog/'.$article->id.'/show') }}">
                                        <div class="user-display-bg"><h2 style="position:absolute; top:2px; left:5%; color:#333; text-shadow: 0 1px 2px rgba(255, 255, 255, 0.6); font-weight: bold">{{ $article->name }}</h2><img src="{{ url('uploads/article/'.$article->id.'.jpg') }}" alt="{{ $article->name }}"></div>
                                        <div class="user-display-bottom">
                                            <div class="user-display-avatar" style="max-width:12%"><img src="{{ url('img/logo.jpg') }}" alt="Avatar"></div>
                                            <div class="row user-content" style="padding-top:50px">
                                            </div>
                                            <div class="row user-display-details">
                                                <div class="col-xs-9">
                                                    <p>{{ $article->description }}</p>
                                                </div>
                                                <div class="col-xs-3">
                                                    <div class="title"><a href="{{ url('blog/'.$article->id.'/show') }}" class="btn btn-block btn-primary btn-md">Lire</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                    <div class="row" style="text-align: center">
                        {{ $articles->links() }}
                    </div>
            </div>
        </div>



@endsection