@extends('layout.default', ['menu' => 'blog'])

@section('title') Blog - {{ $article->name }} @endsection

@section('content')
        <div class="main-content container-fluid">
            <div class="user-profile" style="margin-top:10px">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content"><a style="padding-left:10px" class="btn btn-primary btn-space" href="{{ url('blog') }}"><i class="fa fa-arrow-left"></i> Retour au blog</a>
                                <hr></div>
                        </div>
                        <div class="col-md-12">
                            <div class="user-display">
                                <div class="user-display-bg"><h2 style="position:absolute; top:2px; left:5%; color:#333; text-shadow: 0 1px 2px rgba(255, 255, 255, 0.6); font-weight: bold">{{ $article->name }}</h2><img src="{{ url('uploads/article/'.$article->id.'.jpg') }}" alt="{{ $article->name }}"></div>
                                <div class="user-display-bottom">
                                    <div class="user-display-avatar" style="max-width:12%"><img src="{{ url('img/logo.jpg') }}" alt="Avatar"></div>
                                    <div class="row user-content" style="padding-top:50px">
                                    </div>
                                    <div class="row user-display-details">
                                        <div class="col-md-12">
                                            {!! $article->content !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="comments">
                    <h1 class="display-heading text-center">Commentaires</h1>
                    <hr>
                    <ul class="timeline">
                        @foreach($article->comments()->get() as $comment)
                            <li class="timeline-item timeline-item-detailed">
                                <div class="timeline-date"><span>{{ $comment->created_at->diffForHumans() }}</span></div>
                                <div class="timeline-content">
                                    <div class="timeline-avatar"><img src="@if(!empty($comment->user->avatar)) {{ url('uploads/avatar/'.$comment->user->avatar.'.jpg') }} @else {{ url('assets/img/avatar_140.png') }}@endif" alt="Avatar" class="circle"></div>
                                    <div class="timeline-header"><span class="timeline-time" data-toggle="modal" data-target="#comment-delete">@if($user->bde == 1 or $comment->user->id == $user->id)<a onClick="modalDeleteComment({{ $comment->id }})" href="#"><i style="color: #ea4335" class="mdi mdi-close"></i></a>@endif</span><span class="timeline-autor">{{ $comment->user->lastname.' '.$comment->user->firstname }}  </span>
                                        <p class="timeline-activity">

                                        </p>
                                        <div class="timeline-summary">
                                            <p>{{ $comment->content }}</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                        @if(count($article->comments()->get()) == 0)
                            <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                                <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                                <div class="message">
                                    <strong>Partoutatix!</strong> Il n'y a aucun commentaire sur cet article, pourquoi ne pas en metre un ?
                                </div>
                            </div>
                        @endif
                    </ul>
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-divider">Ajouter un commentaire</div>
                            <div class="panel-body">
                                <form method="POST" action="{{ url('blog/'.$article->id.'/postComment') }}">
                                    {{ csrf_field() }}
                                    <textarea name="comment" placeholder="Entrer votre commentaire ici..." style="width: 100%; max-height:150px; min-height:90px; border-radius: 5px; border-color: #CCC; padding:5px 15px;"></textarea>
                                    <div class="text-right">
                                        <button class="btn btn-primary btn-space btn-xl">Ajouter</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="comment-delete" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <div class="text-danger"><span class="modal-main-icon mdi mdi-comment"></span></div>
                                        <h3>Voulez-vous vraiment supprimer ce commentaire ?</h3>
                                        <h5>Toute suppression est définitive !</h5>
                                        <div class="xs-mt-50">

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Annuler</button>
                                    <a href="" id="modalURLDeleteComment" class="btn btn-danger">Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection

@section('js')
    <script src="{{ url('assets/lib/jquery.magnific-popup/jquery.magnific-popup.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/masonry/masonry.pkgd.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-page-gallery.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
    <script type="text/javascript">

        function modalDeleteComment(id){
            $('#modalURLDeleteComment').attr('href', '{{ url('delete/comment') }}/'+id);
        }
    </script>
@endsection