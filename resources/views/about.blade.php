@extends('layout.default', ['menu' => 'pass'])

@section('title') Qui sommes nous @endsection

@section('content')
        <div class="main-content container-fluid">
        <div class="user-profile" style="margin-top:10px">
            <h2 style="padding-left:10px">Qui sommes nous ?</h2>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="user-display">
                        <div class="user-display-bottom">
                            <div class="row user-display-details">
                                <div class="col-md-12 text-center">
                                    <p>Derrière vos 34 fidèles gaulois se cache une belle bande.</p>
                                    <p>Nous sommes en 2018 après Jésus-Christ. Tout CPE est occupé par les Foxs... Tout ?</p>
                                    <p>Non ! Car une liste peuplée d'irréductibles Gaulois résiste encore et toujours à l'envahisseur.</p>
                                    <p>Et la vie ne s'annonce pas facile pour les autres listes...</p>
                                    <p>Bienvenue sur l’appli officielle de PARTOUT'LALIX'TE !</p>
                                </div>
                                <div class="col-md-12 text-center">
                                    <hr>
                                    <h3 >A venir...</h3>
                                    <h4>Le vendredi 09 février 2018</h4>
                                    <img src="{{ url('img/indice.png') }}" style="max-width: 250px">
                                    <h1 style="font-weight: bold">GET READY !</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
    </script>
@endsection