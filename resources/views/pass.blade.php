@extends('layout.default', ['menu' => 'pass'])

@section('title') Classement @endsection

@section('content')
        <div class="main-content container-fluid">
        <div class="user-profile" style="margin-top:10px">
            <h2 style="padding-left:10px">Mon Pass SkiWix #{{ $user->qrcode_id }}</h2>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div align="center">
                        {!! QrCode::size(250 )->backgroundColor(238,238,238)->generate('http://www.skiwix.fr/qrCode/'.$user->qrcode_id) !!}
                        <p>Appartient à : {{ $user->lastname.' '.$user->firstname }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
    </script>
@endsection