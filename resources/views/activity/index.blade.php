@extends('layout.default', ['menu' => 'activity'])

@section('title') Activités @endsection

@section('content')
        <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Les activités disponibles</h2>
                <hr>
                @foreach($c_activities as $key => $activity)
                <div class="col-md-6">
                    <div class="user-display">
                        <div class="user-display-bg"><img src="{{ url('uploads/activity/'.$activity->id.'.jpg') }}" alt="{{ $activity->name }}"></div>
                        <div class="user-display-bottom">
                            <div class="user-display-avatar"><img src="@if(!empty($activity->user->avatar)) {{ url('uploads/avatar/'.$activity->user->avatar.'.jpg') }} @else {{ 'assets/img/avatar_140.png' }}@endif" alt="Avatar"></div>
                            <div class="user-display-info">
                                <div class="name">{{ $activity->name }}</div>
                                <div class="nick"><span class="mdi mdi-account"></span> {{ $activity->user->firstname.' '.$activity->user->lastname }}</div>
                            </div>
                            <div class="row user-display-details">
                                <div class="col-xs-4">
                                    <div class="title">Equipes</div>
                                    <div class="counter">{{ $activity->team() }}</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">Participants</div>
                                    <div class="counter">{{ $activity->countMembers() }}@if($activity->max_members > 0)/{{ $activity->team()*$activity->max_members }}@endif</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">Status</div>
                                    <div class="counter">
                                        @if($activity->state == 0) <button data-modal="mActivity{{ $key }}" class="btn btn-space btn-info md-trigger"> Rejoindre</button> @elseif($activity->state == 1) <button data-modal="mActivity{{ $key }}" class="btn btn-space btn-warning md-trigger"> En cours</button> @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="mActivity{{ $key }}" class="modal-container colored-header colored-header-primary modal-effect-1">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
                            <h3 class="modal-title">
                                Comment rejoindre cette activité ?<br>
                                <small>Rend-toi à la position ci-dessous muni de ton Pass SkiWix.</small>
                            </h3>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                @if($activity->locationInfo()['type'] == 'text')
                                    <p style="font-size:18px">{{ $activity->locationInfo()['data'] }}</p>
                                @elseif($activity->locationInfo()['type'] == 'map')
                                    <div id="map_{{ $key }}" style="height: 220px;" class="content"></div>
                                    @if(!empty($activity->locationInfo()['data']['text']))
                                        <p style="font-size:18px">{{ $activity->locationInfo()['data']['text'] }}</p>
                                    @endif
                                    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;amp;sensor=false&amp;key=AIzaSyCEp3PcontxMYNAMPYWkWRBGEZqAoFfw9s" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        var locations = [
                                            ['{{ $activity->name }}', {{ $activity->locationInfo()['data']['lat'] }}, {{ $activity->locationInfo()['data']['lat'] }}]
                                        ];

                                        var map = new google.maps.Map(document.getElementById('map_{{ $key }}'), {
                                            zoom: 10,
                                            center: new google.maps.LatLng({{ $activity->locationInfo()['data']['lat'] }}, {{ $activity->locationInfo()['data']['lat'] }}),
                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                        });

                                        var infowindow = new google.maps.InfoWindow();

                                        var marker, i;

                                        for (i = 0; i < locations.length; i++) {
                                            marker = new google.maps.Marker({
                                                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                                map: map
                                            });

                                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                                return function() {
                                                    infowindow.setContent(locations[i][0]);
                                                    infowindow.open(map, marker);
                                                }
                                            })(marker, i));
                                        }
                                    </script>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer"><a href="{{ url('activity/'.$activity->id.'/show') }}" class="btn btn-space btn-info md-trigger"> Voir</a></div>
                    </div>
                </div>
                @endforeach
                @if(count($c_activities) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Pas d'activités prévues à cette heure-ci, reviens plûtot dans l'après midi.
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">
                <h2 style="padding-left:10px">Les activités auxquelles j'ai participé</h2>
                <hr>
                @foreach($p_activities as $key => $activity)
                    <div class="col-md-6">
                        <div class="user-display">
                            <div class="user-display-bg"><img src="{{ url('uploads/activity/'.$activity->id.'.jpg') }}" alt="{{ $activity->name }}"></div>
                            <div class="user-display-bottom">
                                <div class="user-display-avatar"><img src="@if(!empty($activity->user->avatar)) {{ url('uploads/avatar/'.$activity->user->avatar.'.jpg') }} @else {{ 'assets/img/avatar_140.png' }}@endif" alt="Avatar"></div>
                                <div class="user-display-info">
                                    <div class="name">{{ $activity->name }}</div>
                                    <div class="nick"><span class="mdi mdi-account"></span> {{ $activity->user->firstname.' '.$activity->user->lastname }}</div>
                                </div>
                                <div class="row user-display-details">
                                    <div class="col-xs-4">
                                        <div class="title">Equipes</div>
                                        <div class="counter">{{ $activity->team() }}</div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="title">Participants</div>
                                        <div class="counter">{{ $activity->countMembers() }}@if($activity->max_members > 0)/{{ $activity->team()*$activity->max_members }}@endif</div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="title"></div>
                                        <div class="counter">
                                            <a href="{{ url('activity/'.$activity->id.'/show') }}" class="btn btn-space btn-info md-trigger"> Voir</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                @if(count($p_activities) == 0)
                    <div role="alert" class="alert alert-contrast alert-danger alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Partoutatix!</strong> Vous n'avez fait aucune activité pour le moment <i class="fa fa-frown"></i>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
    </script>
@endsection