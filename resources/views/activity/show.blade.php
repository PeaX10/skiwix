@extends('layout.default', ['menu' => 'activity'])

@section('title') Activités - {{ $activity->name }} @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/jquery.magnific-popup/magnific-popup.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/dropzone/dist/dropzone.css') }}"/>
@endsection

@section('content')
        <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            @if($user->bde == 1 && $activity->state >= 0 && $activity->state < 2)
            <div class="row">
                <div class="col-md-12">
                    <div class="user-display">
                        <div class="user-display-bottom">
                            <div class="row user-display-details">
                                <div class="col-xs-4">
                                    <div class="title">@if($activity->state == 0) <a class="btn btn-success" href="{{ url('admin/activity/'.$activity->id.'/start') }}">Commencer</a>@endif</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">@if($activity->state == 0) <a class="btn btn-warning" href="{{ url('admin/activity/'.$activity->id.'/addMember') }}">Ajouter qqun</a> @endif</div>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-12">
                                    <hr>
                                    @foreach($members = json_decode($activity->members, true) as $key => $team)
                                        <div class="showcase">
                                            <div class="dropdown">
                                                <ul style="display: block; position: relative;" class="dropdown-menu dropdown-menu-primary">
                                                    <li><a style="font-weight: bold" href="#">Equipe {{ $key + 1 }}</a></li>
                                                    <li class="divider"></li>
                                                    @foreach($team as $member)
                                                        <li><a href="{{ url('admin/activity/'.$activity->id.'/deleteUser/'.$member.'/'.$key) }}">{{ App\User::find($member)->firstname.' '.App\User::find($member)->lastname }} <i class="fa fa-trash" style="float:right"></i></a></li>
                                                    @endforeach
                                                    @if(count($team) == 0)
                                                        <li><a href="#">0 membres</a></li>
                                                    @endif
                                                    <li class="divider"></li>
                                                    @if($activity->state == 1)
                                                        <li><a style="font-weight: bold" href="{{ url('admin/activity/'.$activity->id.'/winner/'.$key) }}">Désigner cette équipe comme gagnante</a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
            </div>
                @endif
            <div class="row">
                <div class="col-md-9">
                    <div class="user-display">
                        <div class="user-display-bg"><img src="{{ url('uploads/activity/'.$activity->id.'.jpg') }}" alt="{{ $activity->name }}"></div>
                        <div class="user-display-bottom">
                            <div class="user-display-avatar"><img src="@if(!empty($activity->user->avatar)) {{ url('uploads/avatar/'.$activity->user->avatar.'.jpg') }} @else {{ 'assets/img/avatar_140.png' }}@endif" alt="Avatar"></div>
                            <div class="user-display-info">
                                <div class="name">{{ $activity->name }}</div>
                                <div class="nick"><span class="mdi mdi-account"></span> {{ $activity->user->firstname.' '.$activity->user->lastname }}</div>
                            </div>
                            <div class="row user-display-details">
                                <div class="col-xs-4">
                                    <div class="title">Equipes</div>
                                    <div class="counter">{{ $activity->team() }}</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">Participants</div>
                                    <div class="counter">{{ $activity->countMembers() }}@if($activity->max_members > 0)/{{ $activity->team()*$activity->max_members }}@endif</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">Status</div>
                                    <div class="counter">
                                        @if($activity->state == -1) <button class="btn btn-space btn-warning md-trigger"> Passé</button> @elseif($activity->state == 0) <button class="btn btn-space btn-success md-trigger"> Disponible</button> @elseif($activity->state == 1) <button class="btn btn-space btn-warning md-trigger"> En cours</button> @elseif($activity->state == 2) <button class="btn btn-space btn-danger md-trigger"> Terminé</button> @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Localisation<span class="panel-subtitle">Une activité peut en cacher une autre...</span></div>
                        <div class="panel-body">
                            <div class="text-center">
                                @if($activity->locationInfo()['type'] == 'text')
                                    <p style="font-size:18px">{{ $activity->locationInfo()['data'] }}</p>
                                @elseif($activity->locationInfo()['type'] == 'map')
                                    <div id="map" style="height: 200px;" class="content"></div>
                                    @if(!empty($activity->locationInfo()['data']['text']))
                                        <p style="font-size:18px">{{ $activity->locationInfo()['data']['text'] }}</p>
                                    @endif
                                    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;amp;sensor=false&amp;key=AIzaSyCEp3PcontxMYNAMPYWkWRBGEZqAoFfw9s" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        var locations = [
                                            ['{{ $activity->name }}', {{ $activity->locationInfo()['data']['lat'] }}, {{ $activity->locationInfo()['data']['lat'] }}]
                                        ];

                                        var map = new google.maps.Map(document.getElementById('map'), {
                                            zoom: 10,
                                            center: new google.maps.LatLng({{ $activity->locationInfo()['data']['lat'] }}, {{ $activity->locationInfo()['data']['lat'] }}),
                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                        });

                                        var infowindow = new google.maps.InfoWindow();

                                        var marker, i;

                                        for (i = 0; i < locations.length; i++) {
                                            marker = new google.maps.Marker({
                                                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                                map: map
                                            });

                                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                                return function() {
                                                    infowindow.setContent(locations[i][0]);
                                                    infowindow.open(map, marker);
                                                }
                                            })(marker, i));
                                        }
                                    </script>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($activity->state == 2)
            <div class="row">
                <h1 class="display-heading text-center">Les équipes</h1>
                @foreach($members = json_decode($activity->members, true) as $key => $team)
                    <div class="showcase">
                        <div class="dropdown">
                            <ul style="display: block; position: relative;" @if($activity->winner_team == $key) class="dropdown-menu dropdown-menu-success" @else class="dropdown-menu dropdown-menu-danger"  @endif>
                                <li><a style="font-weight: bold" href="#">Equipe {{ $key + 1 }} </a></li>
                                <li class="divider"></li>
                                @foreach($team as $member)
                                    <li><a>{{ App\User::find($member)->firstname.' '.App\User::find($member)->lastname }}</a></li>
                                @endforeach
                                @if(count($team) == 0)
                                    <li><a>0 membres</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif
            <div class="row">
                <h1 class="display-heading text-center">Photos</h1>
                <h3 class="text-center" style="padding-left:10px"><a href="#" data-toggle="modal" data-target="#uploadPhoto" class="btn btn-space btn-danger btn-xl md-trigger"><i class="fa fa-plus"></i> Ajouter</a> <a href="{{ url('download/photos/'.$activity->id) }}" class="btn btn-space btn-primary btn-xl"><i class="fa fa-download"></i> Télécharger l'album</a></h3>
                <hr>
                <div id="uploadPhoto" class="modal-container colored-header colored-header-danger modal-effect-1">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
                            <h3 class="modal-title">
                                Ajouter des photos ?<br>
                                <small>Ces photos seront uniquement visible sur la page de cette activité.</small>
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form method="post" id="my-awesome-dropzone" action="{{ url('activity/'.$activity->id.'/postPhoto') }}" class="dropzone">
                                {{ csrf_field() }}
                                <div class="dz-message">
                                    <div class="icon"><span class="mdi mdi-cloud-upload"></span></div>
                                    <h4>Fichier jpeg, png, bmp, gif de 2Mo max</h4><span class="note">(Merci de respecter les droits de chacun avant de publier quoi que ce soit)</span>
                                    <div class="dropzone-mobile-trigger needsclick"></div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" onclick="window.location.reload()" class="btn btn-space btn-default modal-close">Fermer</button>
                        </div>
                    </div>
                </div>
                <div class="gallery-container">
                    @foreach($activity->photos()->get() as $photo)
                        <div class="item">
                            <div class="photo">
                                <div class="img"><img src="{{ url('uploads/activity/photo/'.$photo->name) }}" alt="Photo {{ $activity->name }}">
                                    <div class="over">
                                        <div class="info-wrapper">
                                            <div class="info">
                                                <div class="title">Par {{ $photo->user->lastname.' '.$photo->user->firstname }}</div>
                                                <div class="date">{{ $photo->created_at->diffForHumans() }}</div>
                                                <div class="func"><a href="{{ url('download/photo/'.$photo->id) }}"><i class="icon mdi mdi-cloud-download"></i></a><a href="{{ url('uploads/activity/photo/'.$photo->name) }}" class="image-zoom"><i class="icon mdi mdi-search"></i></a> @if($user->bde == 1 or $photo->user->id == $user->id) <a href="#" onClick="modalDeletePhoto({{ $photo->id }})" data-toggle="modal" data-target="#photo-delete"><i class="icon mdi mdi-delete"></i></a> @endif</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if(count($activity->photos()->get()) == 0)
                        <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                            <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                            <div class="message">
                                <strong>Partoutatix!</strong> Il n'y a aucune photo sur cette activité, pourquoi ne pas en metre une ?
                            </div>
                        </div>
                    @endif
                    <div id="photo-delete" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <div class="text-danger"><span class="modal-main-icon mdi mdi-delete"></span></div>
                                        <h3>Voulez-vous vraiment supprimer cette photo ?</h3>
                                        <h5>Toute suppression est définitive !</h5>
                                        <div class="xs-mt-50">

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Annuler</button>
                                    <a href="" id="modalURLDelete" class="btn btn-danger">Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="row" id="comments">
                <h1 class="display-heading text-center">Commentaires</h1>
                <hr>
                <ul class="timeline">
                    @foreach($activity->comments()->get() as $comment)
                    <li class="timeline-item timeline-item-detailed">
                        <div class="timeline-date"><span>{{ $comment->created_at->diffForHumans() }}</span></div>
                        <div class="timeline-content">
                            <div class="timeline-avatar"><img src="@if(!empty($comment->user->avatar)) {{ url('uploads/avatar/'.$comment->user->avatar.'.jpg') }} @else {{ url('assets/img/avatar_140.png') }}@endif" alt="Avatar" class="circle"></div>
                            <div class="timeline-header"><span class="timeline-time" data-toggle="modal" data-target="#comment-delete">@if($user->bde == 1 or $comment->user->id == $user->id)<a href="#" onClick="modalDeleteComment({{ $comment->id }})" href="#"><i style="color: #ea4335" class="mdi mdi-close"></i></a>@endif</span><span class="timeline-autor">{{ $comment->user->lastname.' '.$comment->user->firstname }}  </span>
                                <p class="timeline-activity">

                                </p>
                                <div class="timeline-summary">
                                    <p>{{ $comment->content }}</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    @if(count($activity->comments()->get()) == 0)
                            <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                                <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                                <div class="message">
                                    <strong>Partoutatix!</strong> Il n'y a aucun commentaire sur cet article, pourquoi ne pas en metre un ?
                                </div>
                            </div>
                    @endif
                </ul>
                <div class="col-md-8 col-md-offset-2 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Ajouter un commentaire</div>
                        <div class="panel-body">
                            <form method="POST" action="{{ url('activity/'.$activity->id.'/postComment') }}">
                                {{ csrf_field() }}
                                <textarea name="comment" placeholder="Entrer votre commentaire ici..." style="width: 100%; max-height:150px; min-height:90px; border-radius: 5px; border-color: #CCC; padding:5px 15px;"></textarea>
                                <div class="text-right">
                                    <button class="btn btn-primary btn-space btn-xl">Ajouter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="comment-delete" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-comment"></span></div>
                                    <h3>Voulez-vous vraiment supprimer ce commentaire ?</h3>
                                    <h5>Toute suppression est définitive !</h5>
                                    <div class="xs-mt-50">

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Annuler</button>
                                <a href="" id="modalURLDeleteComment" class="btn btn-danger">Supprimer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.magnific-popup/jquery.magnific-popup.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/masonry/masonry.pkgd.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-page-gallery.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).on('load',function(){
            $.fn.niftyModal('setDefaults',{
                overlaySelector: '.modal-overlay',
                closeSelector: '.modal-close',
                classAddAfterOpen: 'modal-show',
            });
            App.pageGallery();
        });
        Dropzone.options.myAwesomeDropzone = {
            maxFilesize: 2,
            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
        }

        function modalDeletePhoto(id){
            $('#modalURLDelete').attr('href', '{{ url('delete/photo') }}/'+id);
        }

        function modalDeleteComment(id){
            $('#modalURLDeleteComment').attr('href', '{{ url('delete/comment') }}/'+id);
        }
    </script>
@endsection