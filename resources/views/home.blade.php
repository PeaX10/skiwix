@extends('layout.default', ['menu' => 'home'])

@section('title') Accueil @endsection

@section('content')
        <div class="main-content container-fluid">
        @if(count($slides) == 0)
            <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                <div class="message">
                    <strong>Oupss!</strong> Il n'y a pas d'articles publiés pour le moment !
                </div>
            </div>
        @else
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @foreach($slides as $key => $slide)
                    <li data-target="#myCarousel" data-slide-to="{{ $key }}" @if($key == 0) class="active" @endif></li>
                @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner slide" data-interval="3000" data-ride="carousel">
                @foreach($slides as $key => $slide)
                <div class="item @if($key == 0) active @endif">
                    <a href="{{ url('blog/'.$slide->id.'/show') }}">
                        <img src="{{ url('uploads/article/'.($slide->id).'.jpg') }}" alt="{{ $slide->name }}">
                        <div class="carousel-caption">
                            <h3>{{ $slide->name }}</h3>
                            <p>{{ $slide->description }}</p>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        @endif

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Les activités disponibles</h2>
                <hr>
                @foreach($activities as $key => $activity)
                <div class="col-md-6">
                    <div class="user-display">
                        <div class="user-display-bg"><img src="{{ url('uploads/activity/'.$activity->id.'.jpg') }}" alt="{{ $activity->name }}"></div>
                        <div class="user-display-bottom">
                            <div class="user-display-avatar"><img src="@if(!empty($activity->user->avatar)) {{ url('uploads/avatar/'.$activity->user->avatar.'.jpg') }} @else {{ 'assets/img/avatar_140.png' }}@endif" alt="Avatar"></div>
                            <div class="user-display-info">
                                <div class="name">{{ $activity->name }}</div>
                                <div class="nick"><span class="mdi mdi-account"></span> {{ $activity->user->firstname.' '.$activity->user->lastname }}</div>
                            </div>
                            <div class="row user-display-details">
                                <div class="col-xs-4">
                                    <div class="title">Equipes</div>
                                    <div class="counter">{{ $activity->team() }}</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">Participants</div>
                                    <div class="counter">{{ $activity->countMembers() }}@if($activity->max_members > 0)/{{ $activity->team()*$activity->max_members }}@endif</div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="title">Status</div>
                                    <div class="counter">
                                        @if($activity->state == 0) <button data-modal="mActivity{{ $key }}" class="btn btn-space btn-info md-trigger"> Rejoindre</button> @elseif($activity->state == 1) <button data-modal="mActivity{{ $key }}" class="btn btn-space btn-warning md-trigger"> En cours</button> @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="mActivity{{ $key }}" class="modal-container colored-header colored-header-primary modal-effect-1">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
                            <h3 class="modal-title">
                                Comment rejoindre cette activité ?<br>
                                <small>Rend-toi à la position ci-dessous muni de ton Pass SkiWix.</small>
                            </h3>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                @if($activity->locationInfo()['type'] == 'text')
                                    <p style="font-size:18px">{{ $activity->locationInfo()['data'] }}</p>
                                @elseif($activity->locationInfo()['type'] == 'map')
                                    <div id="map_{{ $key }}" style="height: 220px;" class="content"></div>
                                    @if(!empty($activity->locationInfo()['data']['text']))
                                        <p style="font-size:18px">{{ $activity->locationInfo()['data']['text'] }}</p>
                                    @endif
                                    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;amp;sensor=false&amp;key=AIzaSyCEp3PcontxMYNAMPYWkWRBGEZqAoFfw9s" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        var locations = [
                                            ['{{ $activity->name }}', {{ $activity->locationInfo()['data']['lat'] }}, {{ $activity->locationInfo()['data']['lat'] }}]
                                        ];

                                        var map = new google.maps.Map(document.getElementById('map_{{ $key }}'), {
                                            zoom: 10,
                                            center: new google.maps.LatLng({{ $activity->locationInfo()['data']['lat'] }}, {{ $activity->locationInfo()['data']['lat'] }}),
                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                        });

                                        var infowindow = new google.maps.InfoWindow();

                                        var marker, i;

                                        for (i = 0; i < locations.length; i++) {
                                            marker = new google.maps.Marker({
                                                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                                map: map
                                            });

                                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                                return function() {
                                                    infowindow.setContent(locations[i][0]);
                                                    infowindow.open(map, marker);
                                                }
                                            })(marker, i));
                                        }
                                    </script>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default md-close">Fermer</button>
                            <a href="{{ url('activity/'.$activity->id.'/show') }}" data-dismiss="modal" class="btn btn-primary md-close">Voir</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @if(count($activities) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Pas d'activités prévues à cette heure-ci, reviens plûtot dans l'après midi.
                        </div>
                    </div>
                @endif
            </div>
            <h2 style="padding-left:10px">Aperçu du classement</h2>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-fullwidth widget-small">
                            <div class="widget-head xs-pb-30">
                                <div class="title">Podium<br><small style="font-weight: bold;">Pleins de lots à gagner tel qu'un drone, un appareil photo, et des accesoires en tout genre...</small>
                                    <div style="height: 350px; padding: 0px; position: relative; background:url('{{ url('img/podium.png') }}') center bottom no-repeat">
                                        <div style=""></div>
                                    </div>
                                </div>
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:4%;">Position</th>
                                        <th style="width:80%;">Nom</th>
                                        <th style="width:7%;">Bitcouix</th>
                                        <th class="actions"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ranking as $key => $rank)
                                    <tr @if($rank->id == $user->id) class="primary" @elseif($key == 0) style="background: #D9A441; color: #FFF" @elseif($key == 1) style="background: #A8A8A8; color: #FFF" @elseif($key == 2) style="background: #965A38; color: #FFF" @endif>
                                        <td>#{{ $key + 1 }}</td>
                                        <td class="user-avatar"> <img class="img-circle" src="@if(!empty($rank->avatar)) {{ url('uploads/avatar/'.$rank->avatar.'.jpg') }} @else {{ 'assets/img/avatar_140.png' }}@endif" alt="Avatar">{{ $rank->lastname.' '.$rank->firstname }}</td>
                                        <td>{{ $rank->points }}</td>
                                        <td class="actions"><a href="#" class="icon"><i style="color:#4285f4" class="fa fa-eye"></i></a></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
    </script>
@endsection