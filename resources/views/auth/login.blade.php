<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Ski Wix - Connexion</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
</head>
<body class="be-splash-screen">
<div class="be-wrapper be-login">
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="splash-container">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="panel-heading"><img src="assets/img/logo-xx.png" alt="logo" width="204" height="54" class="logo-img">
                        @if($errors->count() > 0)<span class="splash-description text-danger">Cette combinaison e-mail / mot de passe n'est pas valide !</span>@endif
                    </div>
                    <div class="panel-body">
                        <form action="/login" method="post">
                            {!! csrf_field() !!}
                            <div class="login-form">
                                <div class="form-group">
                                    <input id="username" name="email" type="email" placeholder="E-mail" autocomplete="on" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input id="password" name="password" type="password" placeholder="Mot de passe" class="form-control">
                                </div>
                                <div class="form-group row login-tools">
                                    <div class="col-xs-6 login-remember">
                                        <div class="be-checkbox">
                                            <input type="checkbox" id="remember">
                                            <label for="remember">Se souvenir de moi</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row login-submit">
                                    <div class="col-xs-6">
                                        <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Connexion</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>