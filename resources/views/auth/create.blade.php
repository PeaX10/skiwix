<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ url('assets/img/logo-fav.png') }}">
    <title>SkiWix - Créer un compte</title>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}" type="text/css"/>
</head>
<body class="be-splash-screen">
<div class="be-wrapper be-login be-signup">
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="splash-container sign-up">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="panel-heading"><img src="{{ url('assets/img/logo-xx.png') }}" alt="logo" width="102" height="27" class="logo-img">
                        <span class="splash-description">Compléter les informations.</span></div>
                    <div class="panel-body">
                        @if($errors->count() > 0)
                            @foreach ($errors->all() as $error)
                                <div role="alert" class="alert alert-contrast alert-warning alert-dismissible">
                                    <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
                                    <div class="message">
                                        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>Attention!</strong> {{ $error }}
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <form action="" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-group xs-pt-10" align="center">
                                <img src="{{ url('assets/img/avatar_140.png') }}" alt="Placeholder" class="img-circle xs-mr-10">
                            </div>
                            <div class="form-group xs-pt-10">
                                <input type="file" name="avatar" id="avatar" data-multiple-caption="{count} files selected" class="inputfile">
                                <label for="avatar" class="btn btn-block btn-default btn-xl"> <i class="mdi mdi-upload"></i><span>Ajouter une photo</span></label>
                            </div>
                            <hr>
                            <div class="form-group">
                                <input type="text" name="lastname" required="" placeholder="Nom" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" name="firstname" required="" placeholder="Prénom" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" required="" placeholder="E-mail" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" required="" placeholder="Mot de passe" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirm" required="" placeholder="Confirmer le mot de passe" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" name="room" required="" placeholder="Chambre" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group xs-pt-10">
                                <button type="submit" class="btn btn-block btn-primary btn-xl">Enregistrer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/js/main.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/jquery.nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/bootstrap-slider/js/bootstrap-slider.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.formElements();
    });
</script>
</body>
</html>