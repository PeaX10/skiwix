<div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Off Canvas Menu</a>
    <div class="left-sidebar-spacer">
        <div class="left-sidebar-scroll">
            <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                    <li class="divider">Menu</li>
                    <li @if($menu == 'home') class="active" @endif><a href="{{ url('/') }}"><i class="icon mdi mdi-home"></i><span>Accueil</span></a>
                    </li>
                    <li @if($menu == 'blog') class="active" @endif><a href="{{ url('blog') }}"><i class="icon mdi mdi-library"></i><span>Blog</span></a>
                    </li>
                    <li @if($menu == 'activity') class="active" @endif><a href="{{ url('activity') }}"><i class="icon mdi mdi-calendar"></i><span>Activités</span></a>
                    </li>
                    <li @if($menu == 'rank') class="active" @endif><a href="{{ url('rank') }}"><i class="icon fa fa-trophy"></i><span>Classement</span></a>

                    </li>
                    <li @if($menu == 'shop') class="active" @endif><a href="{{ url('shop') }}"><i class="icon fa fa-shopping-basket"></i><span>Shopping</span></a>
                    </li>
                    <li @if($menu == 'pass') class="active" @endif><a href="{{ url('pass') }}"><i class="icon fa fa-qrcode"></i><span>Mon Pass SkiWix</span></a>
                    </li>
                    <li @if($menu == 'about') class="active" @endif><a href="{{ url('about') }}"><i class="icon fa fa-question"></i><span>Qui sommes nous ?</span></a>
                    </li>
                    <li @if($menu == 'urgence') class="active" @endif><a href="tel:+33754477610"><i class="icon fa fa-bell"></i><span>Contact d'urgence !</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>