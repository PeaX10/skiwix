<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ url('assets/img/logo-fav.png') }}">
    <title>Skiwix - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}" type="text/css"/>
</head>
<body>
<div class="be-wrapper be-offcanvas-menu be-fixed-sidebar">
    <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <div class="container-fluid">
            <div class="navbar-header"><a href="#" class="be-toggle-left-sidebar"><span class="icon mdi mdi-menu"></span></a><a href="{{ url('/') }}" class="navbar-brand"></a>
            </div>
            <div class="be-right-navbar">
                <ul class="nav navbar-nav navbar-right be-user-nav">
                    <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="@if(!empty($user->avatar)) {{ url('uploads/avatar/'.$user->avatar.'.jpg') }} @else {{ url('assets/img/avatar_140.png') }}@endif" alt="Avatar"><span class="user-name">{{ $user->lastname.' '.$user->firstname }}</span></a>
                        <ul role="menu" class="dropdown-menu">
                            <li>
                                <div class="user-info">
                                    <div class="user-name"><img src="@if(!empty($user->avatar)) {{ url('uploads/avatar/'.$user->avatar.'.jpg') }} @else {{ url('assets/img/avatar_140.png') }}@endif" alt="Avatar" class="img-circle" style="max-width: 40px"> {{ $user->lastname.' '.$user->firstname }}</div>
                                    <div class="user-position away"> {{ $user->points }} <i class="fab fa-bitcoin"></i> Bitcouix</div>
                                </div>
                            </li>
                            <li><a href="{{ url('update/photo') }}"><span class="icon mdi mdi-face"></span> Changer ma photo</a></li>
                            <li><a href="{{ url('logout') }}"><span class="icon mdi mdi-power"></span> Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="page-title"><span>@yield('title')</span></div>

                <ul class="nav navbar-nav navbar-right be-icons-nav">
                    <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">@if($user->bde == 1) <span class="icon mdi mdi-apps"></span> @else <img class="hidden-md hidden-lg" src="{{ url('assets/img/logo-white.png') }}" alt="Partout'lalix'te">@endif</a>
                        @if($user->bde == 1)
                        <ul class="dropdown-menu be-connections">
                            <li>
                                <div class="list">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-xs-4"><a href="{{ url('admin/activity') }}" class="connection-item"><img src="{{ url('img/add_activity.png') }}" alt="Activités"><span>Activités</span></a></div>
                                            <div class="col-xs-4"><a href="tel:+33611815811" class="connection-item"><img src="{{ url('img/help.png') }}" alt="Appeller Alex"><span>Appeller Alex</span></a></div>
                                            <div class="col-xs-4"><a href="{{ url('admin/shop') }}" class="connection-item"><img src="{{ url('img/shopping.png') }}" alt="Commandes"><span>Commandes</span></a></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4"><a href="{{ url('admin/bitcouix/add') }}" class="connection-item"><img src="{{ url('img/bitcouix.png') }}" alt="Ajouter des bitcouix"><span>Ajouter des bitcouix</span></a></div>
                                            <div class="col-xs-4"><a href="{{ url('admin/planning') }}" class="connection-item"><img src="{{ url('img/planning.png') }}" alt="Mon Planning"><span>Mon Planning</span></a></div>
                                            <div class="col-xs-4"><a href="{{ url('admin/blog') }}" class="connection-item"><img src="{{ url('img/blog.png') }}" alt="Articles"><span>Articles</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        @else
                            <ul class="dropdown-menu be-connections">
                                <li>
                                    <div class="list">
                                        <h3 style="text-align:center"><b>Voter Partout'lalix'te !</b></h3>
                                    </div>
                                </li>
                            </ul>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="be-left-sidebar">
        @include('layout.menu_left')
    </div>
    <div class="be-content">
        @yield('content')
    </div>
</div>
<script src="{{ url('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/js/main.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/prettify/prettify.js') }}" type="text/javascript"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
@yield('js')
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();

        //Runs prettify
        prettyPrint();
    });

    $(document).ready(function() {

        // Detect ios 11_x_x affected
        // NEED TO BE UPDATED if new versions are affected
        var ua = navigator.userAgent,
            iOS = /iPad|iPhone|iPod/.test(ua),
            iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);

        // ios 11 bug caret position
        if ( iOS && iOS11 ) {

            // Add CSS class to body
            $("body").addClass("iosBugFixCaret");

        }

    });
</script>
</body>
</html>