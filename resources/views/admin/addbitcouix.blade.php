@extends('layout.default', ['menu' => 'home'])

@section('title') Ajouter des bitcouix @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/bootstrap-slider/css/bootstrap-slider.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/jquery.gritter/css/jquery.gritter.css') }}"/>
@endsection

@section('content')
    <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-fullwidth widget-small">
                        <div class="widget-head xs-pb-30">
                                <form method="POST" action="{{ url('admin/bitcouix/add') }}" class="col-md-8 col-md-offset-2">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">Utilisateur</label>
                                        </div>
                                        <div class="col-sm-3 xs-pt-15">
                                            <select name="user" class="user">
                                                @foreach($users as $user2)
                                                    <option value="{{ $user2->id }}">{{ $user2->firstname.' '.$user2->lastname }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">Nombre de bitcouix: <span id="bitcouix">50</span></label>
                                        </div>
                                        <div class="col-sm-3 xs-pt-15">
                                            <input id="bitcouix_slider" type="text" value="50" name="bitcouix" class="bslider form-control col-sm-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" href="#" id="submit" class="btn btn-success btn-space btn-lg">Ajouter !</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/bootstrap-slider/js/bootstrap-slider.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/jquery.gritter/js/jquery.gritter.js') }}" type="text/javascript"></script>
    <script>
        $("#bitcouix_slider").slider({
            value: 50,
            min: 1,
            max: 250
        }).on("slide",function(e){
            $("#bitcouix").html(e.value);
        });
        $(".user").select2({
            width: '100%'
        });
    </script>
    @if($errors->any())
        <script>
            $.gritter.add({
                title: 'Félicitations',
                text: 'Les bitcouix ont été ajoutés avec succès !',
                class_name: 'color success'
            });
        </script>
    @endif
@endsection