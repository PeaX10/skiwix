@extends('layout.default', ['menu' => 'blog'])

@section('title') Ajouter un article @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/bootstrap-slider/css/bootstrap-slider.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/summernote/summernote.css') }}"/>
@endsection

@section('content')
    <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-fullwidth widget-small">
                        <div class="widget-head xs-pb-30">
                                <form method="POST" action="{{ url('admin/blog') }}" class="col-md-8 col-md-offset-2" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    @if($errors->count() > 0)
                                        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
                                            <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
                                            <div class="message">
                                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>Attention!</strong> Merci de remplir entierement ce formulaire
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">Titre</label>
                                        </div>
                                        <div class="col-sm-3 xs-pt-15">
                                            <input type="text" class="form-control input-sm" name="name" value="@if(!empty(old('name'))) {{ old('name') }} @endif">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">En ligne ?</label>
                                        </div>
                                        <div class="col-sm-3 xs-pt-15">
                                            <div class="switch-button switch-button-success">
                                                <input type="checkbox" @if(!empty(old('state'))) checked @endif name="state" id="state"><span>
                                                <label for="state"></label></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label for="img" class="control-label">Image</label>
                                        </div>
                                        <div class="col-sm-3 xs-pt-15">
                                            <input type="file" name="image" id="image" data-multiple-caption="{count} files selected" class="inputfile">
                                            <label for="image" class="btn btn-block btn-default btn-xl"> <i class="mdi mdi-upload"></i><span>Ajouter une photo</span></label>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">Description</label>
                                        </div>
                                        <div class="col-sm-3 xs-pt-15">
                                            <textarea id="description" type="text" placeholder="Petite descrption rapide qui donne envie de lire l'article" name="description" class="form-control col-sm-12">@if(!empty(old('description'))) {{ old('description') }}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="margin-top:20px">
                                            <div class="panel panel-default panel-border-color panel-border-color-primary">
                                                <div class="panel-heading panel-heading-divider">Contenu de l'article</div>
                                                <div class="panel-body">
                                                    <textarea name="content" id="editor1">
                                                        @if(!empty(old('content'))) {{ old('content') }} @endif
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" href="#" id="submit" class="btn btn-success btn-space btn-lg">Ajouter !</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/summernote/summernote.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/summernote/summernote-ext-beagle.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/markdown-js/markdown.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script>
        App.textEditors();
        $("#bitcouix_slider").slider({
            value: 50,
            min: 1,
            max: 250
        }).on("slide",function(e){
            $("#bitcouix").html(e.value);
        });
        $(".user").select2({
            width: '100%'
        });
        App.formElements();
    </script>
@endsection