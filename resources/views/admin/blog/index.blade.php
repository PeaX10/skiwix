@extends('layout.default', ['menu' => 'blog'])

@section('title') Listes des articles @endsection

@section('content')
        <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Articles</h2>
                <div class="col-sm-12">
                    <a href="{{ url('admin/blog/create') }}" class="btn btn-danger btn-space">Ajouter</a>
                </div>
                <hr>
                <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-body">
                            <table id="table1" class="table table-striped table-hover table-responsive" st>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Titre</th>
                                    <th>Statut</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $key => $article)
                                    <tr>
                                        <td>#{{ $article->id }}</td>
                                        <td>{{ $article->name }}</td>
                                        <td>
                                            @if($article->state == 0)
                                                <button class="btn btn-space btn-warning"><i class="fa fa-clock"></i></button>
                                            @else
                                                <button class="btn btn-space btn-success"><i class="fa fa-check"></i></button>
                                            @endif
                                        </td>
                                        <td class="center">{{ $article->created_at->diffForHumans() }}</td>
                                        <td><a data-toggle="tooltip" data-placement="left" data-original-title="Voir" href="{{ url('blog/'.$article->id.'/show') }}"><i class="fa fa-eye"></i></a><a data-toggle="tooltip" data-placement="left" data-original-title="Modifier" class="text-warning" href="{{ url('admin/blog/'.$article->id.'/edit') }}"><i class="fa fa-edit"></i></a> <a data-toggle="tooltip" data-toggle="modal" data-target="#deleteArticle" onclick="Delete( {{ $article->id }} )" data-placement="left" data-original-title="Supprimer" href="#" class="text-danger"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(count($articles) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Pas d'article pour le moment
                        </div>
                    </div>
                @endif
                <div id="deleteArticle" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-delete"></span></div>
                                    <h3>Voulez-vous vraiment supprimer cet article ?</h3>
                                    <h5>Toute suppression est définitive !</h5>
                                    <div class="xs-mt-50">

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Annuler</button>
                                <a href="" id="url" class="btn btn-danger">Supprimer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
        App.dataTables();
        function Delete(id){
            $("#deleteArticle").modal();
            $('#deleteArticle #url').attr('href', '{{ url('admin/article/') }}/' + id + '/delete');
        }
    </script>
@endsection