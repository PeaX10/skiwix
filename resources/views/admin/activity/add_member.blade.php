@extends('layout.default', ['menu' => 'activity'])

@section('title') Choisir une équipe @endsection

@section('content')
    <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Choisir une équipe</h2>
                <hr>
                <div class="col-sm-12">
                    @foreach($members as $key => $team)
                        <div class="showcase">
                            <div class="dropdown">
                                <ul style="display: block; position: relative;" class="dropdown-menu dropdown-menu-primary">
                                    <li><a href="#">Equipe {{ $key + 1 }}</a></li>
                                    <li class="divider"></li>
                                    @foreach($team as $member)
                                        <li><a href="#">{{ $member }}</a></li>
                                    @endforeach
                                    @if(count($team) == 0)
                                        <li><a href="#">0 membres</a></li>
                                    @endif
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/activity/'.$activity->id.'/addUser/'.$qr_user->id.'/'.$key) }}">Ajouter à cette équipe</a></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
        App.dataTables();
        function Delete(id){
            $("#deleteActivity").modal();
            $('#deleteActivity #url').attr('href', '{{ url('admin/activity/') }}/' + id + '/delete');
        }
    </script>
@endsection