@extends('layout.default', ['menu' => 'activity'])

@section('title') Créer une activité @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/bootstrap-slider/css/bootstrap-slider.css') }}"/>
    <style>
        .be-radio-icon input[type="radio"]:checked + label{
            border-width:2px;
        }
    </style>
@endsection

@section('content')
    <div class="main-content container-fluid">
        <div class="row wizard-row">
            <div class="col-md-12 fuelux">
                <div class="block-wizard panel panel-default">
                    <div id="wizard1" class="wizard wizard-ux">
                        <ul class="steps">
                            <li data-step="1" class="active">Etape 1<span class="chevron"></span></li>
                            <li data-step="2">Etape 2<span class="chevron"></span></li>
                            <li data-step="3">Etape 3<span class="chevron"></span></li>
                        </ul>
                        <div class="actions">
                            <button type="button" class="btn btn-xs btn-prev btn-default"><i class="icon mdi mdi-chevron-left"></i>Prev</button>
                            <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-default">Next<i class="icon mdi mdi-chevron-right"></i></button>
                        </div>
                        <form action="{{ url('admin/activity') }}" method="POST" data-parsley-namespace="data-parsley-" class="form-horizontal group-border-dashed" id="form">
                            {{ csrf_field() }}
                            <div class="step-content">
                                @if(count($errors) > 0)
                                <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
                                    <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
                                    <div class="message">
                                        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>Attention!</strong> Merci de compléter correctement le formulaire.
                                    </div>
                                </div>
                                @endif
                            <div data-step="1" class="step-pane active">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7">
                                        <h3 class="wizard-title">Conditions</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Utilisateurs pouvant gérer cette activité</label>
                                    <div class="col-sm-6">
                                        <select multiple="" name="helpers[]" class="tags">
                                            @foreach(App\User::where('bde', 1)->get() as $user2)
                                                <option value="{{ $user->id }}">{{ $user2->firstname.' '.$user2->lastname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Localisation</label>
                                    <div class="col-sm-6">
                                        <div id="map" style="height: 200px;" class="content"></div>
                                        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;amp;sensor=false&amp;key=AIzaSyCEp3PcontxMYNAMPYWkWRBGEZqAoFfw9s" type="text/javascript"></script>
                                        <script type="text/javascript">
                                            var locations = [
                                                ['Mon activité', 45.2561272, 5.172653]
                                            ];

                                            var map = new google.maps.Map(document.getElementById('map'), {
                                                zoom: 10,
                                                center: new google.maps.LatLng(45.2561272, 5.172653),
                                                mapTypeId: google.maps.MapTypeId.ROADMAP
                                            });

                                            var infowindow = new google.maps.InfoWindow();

                                            var marker, i;

                                            for (i = 0; i < locations.length; i++) {
                                                marker = new google.maps.Marker({
                                                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                                    map: map,
                                                    draggable: true,
                                                    animation: google.maps.Animation.DROP
                                                });

                                                marker.addListener('drag', handleEvent);
                                                marker.addListener('dragend', handleEvent);

                                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                                    return function() {
                                                        infowindow.setContent(locations[i][0]);
                                                        infowindow.open(map, marker);
                                                    }
                                                })(marker, i));
                                            }

                                            function handleEvent(event) {
                                                document.getElementById('lat').value = event.latLng.lat();
                                                document.getElementById('lng').value = event.latLng.lng();
                                            }
                                        </script>
                                        <input type="hidden" name="lat" id="lat" value="45.2561272">
                                        <input type="hidden" id="lng" name="lng" value="5.172653">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Info sur le lieu</label>
                                    <div class="col-sm-6">
                                        <textarea type="text" name="locText" placeholder="Dans le hall..." class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <a href="{{ url('admin/activity') }}" class="btn btn-default btn-space">Annuler</a>
                                        <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Etape suivante</button>
                                    </div>
                                </div>
                            </div>
                            <div data-step="2" class="step-pane">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7">
                                        <h3 class="wizard-title">Quel type d'activité voulez vous lancer ?</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label xs-pt-20">Type</label>
                                    <div class="col-sm-6">
                                        @for($k=1; $k <= 9; $k++)
                                            <div class="be-radio-icon inline">
                                                <input type="radio" checked="" name="activity_id" id="rad{{ $k }}" value="{{ $k }}">
                                                <label for="rad{{ $k }}"><img src="{{ url('uploads/activity/'.$k.'.png') }}" style="min-width: 200px; max-width:100%"></label>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <a href="{{ url('admin/activity') }}" class="btn btn-default btn-space">Annuler</a>
                                        <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Etape suivante</button>
                                    </div>
                                </div>
                            </div>
                            <div data-step="3" class="step-pane">
                                <div class="form-group no-padding">
                                    <div class="col-sm-7">
                                        <h3 class="wizard-title">Configuration</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-7">
                                        <label class="control-label">Points perdant: <span id="looserPts">50</span> Bitcouix</label>
                                    </div>
                                    <div class="col-sm-3 xs-pt-15">
                                        <input id="looserPts_slider" type="text" value="50" name="point_participant" class="bslider form-control col-sm-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-7">
                                        <label class="control-label">Points gagant: <span id="winnerPts">100</span> Bitcouix</label>
                                    </div>
                                    <div class="col-sm-3 xs-pt-15">
                                        <input id="winnerPts_slider" type="text" value="100" name="point_winner" class="bslider form-control col-sm-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-7">
                                        <label class="control-label">Nombre d'équipe: <span id="team">2</span></label>
                                    </div>
                                    <div class="col-sm-3 xs-pt-15">
                                        <input id="team_slider" type="text" value="2" name="team" class="bslider form-control col-sm-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-7">
                                        <label class="control-label">Membres max par équipe: <span id="maxMembers">15</span></label>
                                    </div>
                                    <div class="col-sm-3 xs-pt-15">
                                        <input id="maxMembers_slider" type="text" value="15" name="max_members" class="bslider form-control col-sm-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button data-wizard="#wizard1" class="btn btn-default btn-space wizard-previous">Précédant</button>
                                        <a type="submit" href="#" onclick="submit()" id="submit" class="btn btn-success btn-space wizard-next">Lancer !</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/bootstrap-slider/js/bootstrap-slider.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-form-wizard.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            App.wizard();
        });
        function submit(){
            $('#form').submit();
        }
    </script>
@endsection