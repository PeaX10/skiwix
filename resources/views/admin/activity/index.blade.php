@extends('layout.default', ['menu' => 'activity'])

@section('title') Listes des utilisateurs @endsection

@section('content')
        <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Activités</h2>
                <div class="col-sm-12">
                    <a href="{{ url('admin/activity/create') }}" class="btn btn-danger btn-space">Ajouter</a>
                </div>
                <hr>
                <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-body">
                            <table id="table1" class="table table-striped table-hover table-responsive" st>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Nom</th>
                                    <th>Hôte</th>
                                    <th>Date</th>
                                    <th>Statut</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($activities as $key => $activity)
                                    <tr class="odd gradeX">
                                        <td>#{{ $activity->id }}</td>
                                        <td>{{ $activity->name }}</td>
                                        <td>{{ $activity->user->firstname.' '.$activity->user->lastname }}</td>
                                        <td class="center">{{ $activity->created_at->diffForHumans() }}</td>
                                        <td>@if($activity->state == 0) En attende de participants @elseif($activity->state == 1) En cours @elseif($activity->state == -1) Terminé </button> @elseif($activity->state == -2) En création @endif</td>
                                        <td><a data-toggle="tooltip" data-placement="left" data-original-title="Voir" href="{{ url('activity/'.$activity->id.'/show') }}"><i class="fa fa-eye"></i></a> <a href="{{ url('admin/activity/'.$activity->id.'/clone') }}" data-toggle="tooltip" data-placement="left" data-original-title="Dupliquer" href="#" class="text-warning"><i class="fa fa-clone"></i></a> <a data-toggle="tooltip" data-toggle="modal" data-target="#deleteActivity" onclick="Delete( {{ $activity->id }} )" data-placement="left" data-original-title="Supprimer" href="#" class="text-danger"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(count($activities) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Aucune activité
                        </div>
                    </div>
                @endif
                <div id="deleteActivity" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-delete"></span></div>
                                    <h3>Voulez-vous vraiment supprimer cette activité ?</h3>
                                    <h5>Toute suppression est définitive !</h5>
                                    <div class="xs-mt-50">

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Annuler</button>
                                <a href="" id="url" class="btn btn-danger">Supprimer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
        App.dataTables();
        function Delete(id){
            $("#deleteActivity").modal();
            $('#deleteActivity #url').attr('href', '{{ url('admin/activity/') }}/' + id + '/delete');
        }
    </script>
@endsection