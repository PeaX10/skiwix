@extends('layout.default', ['menu' => 'shop'])

@section('title') Stock produit @endsection

@section('content')
        <div class="main-content container-fluid">

        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Stocks des produits</h2>
                <div class="col-sm-12">
                    <a href="{{ url('admin/shop') }}" class="btn btn-primary btn-space">Retour aux commandes</a>
                </div>
                <hr>
                <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-body">
                            <p class="text-primary text-center" style="padding-top:15px;font-weight: bold">Petit info des familles: -1 pour stock infini</p>
                            <table id="table1" class="table table-striped table-hover table-responsive" st>
                                <thead>
                                <tr>
                                    <th>Produit</th>
                                    <th>Stock</th>
                                    <th>Dernière mise à jour</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key => $product)
                                    <tr>
                                        <form action="{{ url('admin/shop/'.$product->id.'/stock') }}" method="POST">
                                            {{ csrf_field() }}
                                            <td>
                                                {{ $product->name }}
                                            </td>
                                            <td>
                                                <input style="max-width: 70px" name="stock" type="number" value="{{  $product->stock }}" />
                                            </td>
                                            <td class="center">{{ $product->updated_at->diffForHumans() }}</td>
                                            <td><button type="submit" class="btn btn-block btn-space btn-success">Màj !</button></td>
                                        </form>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(count($products) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Pas de produit pour le moment
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
        App.dataTables();
    </script>
@endsection