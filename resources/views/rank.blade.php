@extends('layout.default', ['menu' => 'rank'])

@section('title') Classement @endsection

@section('content')
        <div class="main-content container-fluid">
        <div class="user-profile" style="margin-top:10px">
            <h2 style="padding-left:10px">Classement global</h2>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-fullwidth widget-small">
                            <div class="widget-head xs-pb-30">
                                <div class="title">Vous êtes classé @foreach($ranking as $key => $rank) @if($user->id == $rank->id) @if($key == 0) {{ '1er' }} @else {{ $key + 1 }}ème @endif sur {{ count($ranking) }} participants @endif @endforeach</div>
                            </div>
                            <div class="widget-chart-container">
                                <div style="height: 350px; padding: 0px; position: relative; background:url('{{ url('img/podium_bg.jpg') }}') center bottom no-repeat">
                                    <div style="height: 350px; padding: 0px; position: relative; background:url('{{ url('img/podium.png') }}') center bottom no-repeat">
                                        <div style=""></div>
                                    </div>
                                </div>
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:4%;">Position</th>
                                        <th style="width:80%;">Nom</th>
                                        <th style="width:7%;">Bitcouix</th>
                                        <th class="actions"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ranking as $key => $rank)
                                    <tr @if($rank->id == $user->id) class="primary" @elseif($key == 0) style="background: #D9A441; color: #FFF" @elseif($key == 1) style="background: #A8A8A8; color: #FFF" @elseif($key == 2) style="background: #965A38; color: #FFF" @endif>
                                        <td>#{{ $key + 1 }}</td>
                                        <td class="user-avatar"> <img class="img-circle" src="@if(!empty($rank->avatar)) {{ url('uploads/avatar/'.$rank->avatar.'.jpg') }} @else {{ 'assets/img/avatar_140.png' }}@endif" alt="Avatar">{{ $rank->lastname.' '.$rank->firstname }}</td>
                                        <td>{{ $rank->points }}</td>
                                        <td class="actions"><a href="#" class="icon"><i style="color:#4285f4" class="fa fa-eye"></i></a></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
    </script>
@endsection