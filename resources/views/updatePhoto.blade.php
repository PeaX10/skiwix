@extends('layout.default', ['menu' => 'home'])

@section('title') Modifier ma photo de profil @endsection

@section('content')
        <div class="main-content container-fluid">
        <div class="user-profile" style="margin-top:10px">
            <h2 style="padding-left:10px">Ma photo de profil</h2>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div align="center">
                        <form action="" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <img src="@if(!empty($user->avatar)) {{ url('uploads/avatar/'.$user->avatar.'.jpg') }} @else {{ url('assets/img/avatar_140.png') }}@endif" style="max-width: 140px">
                            <div class="form-group xs-pt-10">
                                <input type="file" name="avatar" id="avatar" data-multiple-caption="{count} files selected" class="inputfile">
                                <label for="avatar" class="btn btn-block btn-default btn-xl"> <i class="mdi mdi-upload"></i><span>Ajouter une photo</span></label>
                            </div>
                            <button type="submit" class="btn btn-success btn-space">Modifier</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
    </script>
@endsection