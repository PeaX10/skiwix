<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Cards SkiWix</title>

    </head>
    <style>
        .card{
            width:635px;
            height: 410px;
            margin-left:20px;
            border:1px solid #CCC;
            border-radius: 6px;
            margin-bottom: 20px;
            background: url('/img/card_bg2.jpg') no-repeat center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            color: #FFF;
            font-family: Roboto, Arial, sans-serif;
            text-shadow: 1px 1px #000;
            font-weight: bold;
        }

        .infos{
            display: inline-flex;
            font-size:22px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
    <body class="row">
        @for ($j=23; $j < 46; $j++)
            @for ($i = 0; $i < 4; $i++)
                <table>
                    <tr>
                        <td>
                            <div class="card">
                                <h2 style="width: 100%; color: #FFF; background: rgba(0,0,0,0.5); padding:5px; text-align:center">Pass SkiWix #{{ ($j*8 + 2*$i + 1) }}</h2>
                                <div class="infos">
                                    <div>
                                        <img style="max-width:170px" src="{{ url('uploads/qrcode/'.($j*8 + 2*$i + 1).'.png') }}">
                                    </div>
                                    <div style="display: block; list-style: none; margin-left:200px;color:#000; line-height: 28px; background: rgba(255,255,255, 0.8); width: 350px; height: 160px; padding-left:10px; padding-top:5px">
                                        <li>Nom : </li>
                                        <li>Prénom : </li>
                                        <li>Chambre : </li>
                                        <li>E-mail : </li>
                                        <li>Filière : </li>
                                    </div>
                                </div>
                        </td>
                        <td>
                            <div class="card">
                                <h2 style="width: 100%; color: #FFF; background: rgba(0,0,0,0.5); padding:5px; text-align:center">Pass SkiWix #{{ ($j*8 + 2*$i + 2) }}</h2>
                                <div class="infos">
                                    <div>
                                        <img style="max-width:170px" src="{{ url('uploads/qrcode/'.($j*8 + 2*$i + 2).'.png') }}">
                                    </div>
                                    <div style="display: block; list-style: none; margin-left:200px;color:#000; line-height: 28px; background: rgba(255,255,255, 0.8); width: 350px; height: 160px; padding-left:10px; padding-top:5px">
                                        <li>Nom : </li>
                                        <li>Prénom : </li>
                                        <li>Chambre : </li>
                                        <li>E-mail : </li>
                                        <li>Filière : </li>
                                    </div>
                                </div>
                        </td>
                    </tr>
                </table>
            @endfor
            <div class="page-break"></div>
        @endfor

    </body>
</html>
