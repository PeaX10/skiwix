@extends('layout.default', ['menu' => 'shop'])

@section('title') Mes commandes @endsection

@section('content')
        <div class="main-content container-fluid">
            <div class="panel-heading panel-heading-divider"><span class="panel-subtitle text-right"><a href="{{ url('shop/orders') }}"><i class="fa fa-bars"></i> Mes commandes ({{ $user->orders()->count() }})</a> | <a href="{{ url('shop/basket') }}"><i class="fa fa-shopping-basket"></i> Mon panier ({{ count(json_decode(Cookie::get('shop'), true)) }})</a></span></div>
        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Commandes</h2>
                <hr>
                <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-body">
                            <table id="table1" class="table table-striped table-hover table-responsive" st>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Contenu</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $key => $order)
                                    <tr>
                                        <td>#{{ $order->id }}</td>
                                        <td>
                                            @foreach(json_decode($order->data, true) as $product)
                                                <b>{{ $product['quantity'] }}x</b> {{ \App\Product::find($product['product_id'])->name }} @if(!empty($product['option']))(<i>{{ $product['option'] }}</i>)@endif<br>
                                            @endforeach
                                        </td>
                                        <td class="center">{{ $order->created_at->diffForHumans() }}</td>
                                        <td>@if($order->state == 0) <a href="#" class="btn btn-clock btn-space btn-warning">En préparation</a> @else <a href="#" class="btn btn-clock btn-space btn-success">Livré !</a> @endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(count($orders) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Vous n'avez pas fait de commande pour l'instant
                        </div>
                    </div>
                @endif
                <div id="deleteOrder" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-warning"><span class="modal-main-icon mdi mdi-check"></span></div>
                                    <h3>Voulez-vous valider la préparation de cette commande ?</h3>
                                    <h5>Une fois préparer celle-ci est considéré comme livré !</h5>
                                    <div class="xs-mt-50">

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Annuler</button>
                                <a href="" id="url" class="btn btn-warning">Traiter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });

        function Delete(id){
            $("#deleteOrder").modal();
            $('#deleteOrder #url').attr('href', '{{ url('admin/shop/') }}/' + id + '/delete');
        }
    </script>
@endsection