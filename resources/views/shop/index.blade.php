@extends('layout.default', ['menu' => 'shop'])

@section('title') Shop @endsection

@section('content')
        <div class="main-content container-fluid">
            <div class="user-profile" style="margin-top:10px">
                    <div class="row">
                        <h2 style="padding-left:10px">Les produits disponible en livraison Idefix</h2>
                        <div class="panel-heading panel-heading-divider"><span class="panel-subtitle text-right"><a href="{{ url('shop/orders') }}"><i class="fa fa-bars"></i> Mes commandes ({{ $user->orders()->count() }})</a> | <a href="{{ url('shop/basket') }}"><i class="fa fa-shopping-basket"></i> Mon panier ({{ count(json_decode(Cookie::get('shop'), true)) }})</a></span></div>
                        <hr>
                        @foreach($products as $key => $product)
                            <div class="col-md-6">
                                <div class="user-display">
                                    <a href="{{ url('shop/'.$product->id.'/show') }}">
                                        <div class="user-display-bg"><h2 style="color:#4285f4; text-shadow: 0 1px 2px rgba(255, 255, 255, 0.6); font-weight: bold; padding-left:10px;">{{ $product->name }}</h2><img src="{{ url('uploads/product/'.$product->id.'.jpg') }}" alt="{{ $product->name }}"></div>
                                        <div class="user-display-bottom">
                                            <div class="row user-display-details">
                                                <div class="col-md-9 col-xs-12">
                                                    <p>{{ $product->description }}</p>
                                                </div>
                                                <div class="col-md-3 col-xs-12">
                                                    <div class="title"><a href="{{ url('shop/'.$product->id.'/show') }}" class="btn btn-block btn-primary btn-md">Commander</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        @if(count($products) == 0)
                            <div role="alert" class="alert alert-contrast alert-danger alert-dismissible">
                                <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                                <div class="message">
                                    <strong>Oups!</strong>  Pas de produit encore disponible !
                                </div>
                            </div>
                        @endif
                    </div>
                    <hr>
            </div>
        </div>



@endsection