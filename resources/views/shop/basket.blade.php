@extends('layout.default', ['menu' => 'shop'])

@section('title') Mon panier @endsection

@section('content')
        <div class="main-content container-fluid">
            <div class="panel-heading panel-heading-divider"><span class="panel-subtitle text-right"><a href="{{ url('shop/orders') }}"><i class="fa fa-bars"></i> Mes commandes ({{ $user->orders()->count() }})</a> | <a href="{{ url('shop/basket') }}"><i class="fa fa-shopping-basket"></i> Mon panier ({{ count(json_decode(Cookie::get('shop'), true)) }})</a></span></div>
        <div class="user-profile" style="margin-top:10px">
            <div class="row">
                <h2 style="padding-left:10px">Commandes</h2>
                <hr>
                <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-body">
                            <table id="table1" class="table table-striped table-hover table-responsive" st>
                                <thead>
                                <tr>
                                    <th>Produit</th>
                                    <th>Quantité</th>
                                    <th>Option</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $key => $order)
                                    <tr>
                                        <td>{{ \App\Product::find($order['product_id'])->name }}</td>
                                        <td>{{ $order['quantity'] }}</td>
                                        <td class="center">@if(!empty($product['option']))(<i>{{ $product['option'] }}</i>)@endif</td>
                                        <td><a href="{{ url('shop/'.$key.'/delete') }}" class="btn btn-block btn-space btn-danger"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <h3>Déguissement</h3>
                    <form action="{{ url('shop/basket') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="col-sm-8">
                            <select class="form-control" name="skin" id="skin">
                                <option value="0">Peu importe</option>
                                <option value="1">Egyptien</option>
                                <option value="2">Barde</option>
                            </select>
                        </div>
                        <div class="col-sm-4"><button type="submit" class="btn btn-lg btn-space btn-success">Commander</button></div>

                    </form>
                </div>
                @if(count($orders) == 0)
                    <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Oupss!</strong> Vous n'avez rien dans votre panier pour le moment
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>



@endsection
@section('js')
    <script src="{{ url('assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });

        function Delete(id){
            $("#deleteOrder").modal();
            $('#deleteOrder #url').attr('href', '{{ url('admin/shop/') }}/' + id + '/delete');
        }
    </script>
@endsection