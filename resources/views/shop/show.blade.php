@extends('layout.default', ['menu' => 'shop'])

@section('title') Produit - {{ $product->name }} @endsection

@section('content')
        <div class="main-content container-fluid">
            <div class="user-profile" style="margin-top:10px">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="padding-left:10px">Les produits disponible en livraison Idefix</h2>
                            <div class="panel-heading panel-heading-divider"><span class="panel-subtitle text-right"><a href="{{ url('shop/orders') }}"><i class="fa fa-bars"></i> Mes commandes ({{ $user->orders()->count() }})</a> | <a href="{{ url('shop/basket') }}"><i class="fa fa-shopping-basket"></i> Mon panier ({{ count(json_decode(Cookie::get('shop'), true)) }})</a></span></div>
                        </div>
                        <div class="col-md-12">
                            <div class="user-display">
                                <div class="user-display-bottom">
                                    <div class="row user-display-details">
                                        <div class="col-md-12 text-center">
                                            <h2 class="col-md-12 text-left" style="color: #4285f4; text-shadow: 0 1px 2px rgba(255, 255, 255, 0.6); font-weight: bold; padding-bottom: 20px; padding-left:10px">{{ $product->name }}</h2>
                                            <hr>
                                            <div class="col-md-8 col-xs-12">
                                                <img class="img-rounded" style="max-width:100%" src="{{ url('uploads/product/'.$product->id.'.jpg') }}" alt="{{ $product->name }}">
                                                <p>{!! $product->description !!}</p>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <form class="form-horizontal" action="{{ url('shop/postOrder') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                    @if(count($options) > 0)
                                                    <div class="form-group">
                                                        <label for="option" class="col-sm-4 control-label">Option</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" name="option">
                                                                @foreach($options as $option)
                                                                    <option value="{{ $option }}">{{ $option }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if(($product->daily() < $product->daily_max or $product->daily_max == 0) && ($product->stock != 0))
                                                        <div class="form-group">
                                                            <label for="option" class="col-sm-4 control-label">Quantité</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control" name="quantity">
                                                                    @if($product->stock > $product->daily_max - $product->daily() or $product->stock == -1))
                                                                        @for($i=1; $i<=($product->daily_max - $product->daily()); $i++)
                                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                                        @endfor
                                                                    @elseif($product->stock == -1)
                                                                        @for($i=1; $i<=(10); $i++)
                                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                                        @endfor
                                                                    @else
                                                                        @for($i=1; $i<=($product->stock); $i++)
                                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                                        @endfor
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-xl btn-block text-center">Ajouter au panier</button>
                                                    @else
                                                        <div role="alert" class="alert alert-contrast alert-danger alert-dismissible">
                                                            <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                                                            <div class="message">
                                                                <strong>Partoutatix!</strong> Il semblerait que en as déjà pas mal commandé aujourd'hui !
                                                            </div>
                                                        </div>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>



@endsection